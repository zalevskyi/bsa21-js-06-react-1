import { Chat } from './components/Chat';

function App() {
  return (
    <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
  );
}

export default App;
