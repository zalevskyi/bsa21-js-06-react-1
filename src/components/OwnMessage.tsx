import React from 'react';
import '../styles/OwnMessage.css';
import { BasicMessage } from './BasicMessage';

export class OwnMessage extends React.Component<{
  id: string;
  text: string;
  time: string;
  edit: Function;
  delete: Function;
}> {
  render() {
    return (
      <div className='own-message'>
        <BasicMessage text={this.props.text} time={this.props.time} />
        <Button
          id={this.props.id}
          onClick={this.props.edit}
          name='Edit'
          className='message-edit'
        />
        <Button
          id={this.props.id}
          onClick={this.props.delete}
          name='Delete'
          className='message-delete'
        />
      </div>
    );
  }
}

class Button extends React.Component<{
  id: string;
  onClick: Function;
  name: string;
  className: string;
}> {
  render() {
    return (
      <button
        type='button'
        className={this.props.className}
        onClick={e => {
          this.props.onClick(this.props.id);
        }}>
        {this.props.name}
      </button>
    );
  }
}
