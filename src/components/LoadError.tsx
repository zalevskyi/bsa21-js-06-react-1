import React from 'react';
import '../styles/LoadError.css';

interface LoadErrorProps {
  message: string;
}

export class LoadError extends React.Component<LoadErrorProps> {
  render() {
    return <div className='load-error'>{this.props.message}</div>;
  }
}
