import React from 'react';
import '../styles/MessageList.css';
import { MessageData } from './Chat';
import { Message } from './Message';
import { OwnMessage } from './OwnMessage';

export class MessageList extends React.Component<{
  currentUserId: string;
  messages: MessageData[];
  toggleLike: Function;
  edit: Function;
  delete: Function;
}> {
  render() {
    const messagesByDate = splitByDates(this.props.messages);
    return (
      <div className='message-list'>
        {messagesByDate.map(date => {
          const [dateValue, dateMessages] = date;
          return (
            <MessageListFragment
              currentUserId={this.props.currentUserId}
              date={dateValue}
              messages={dateMessages}
              toggleLike={this.props.toggleLike}
              edit={this.props.edit}
              delete={this.props.delete}
              key={dateValue}
            />
          );
        })}
      </div>
    );
  }
}

class MessageDivider extends React.Component<{ date: string }> {
  render() {
    return (
      <div className='messages-divider' key={this.props.date}>
        {bsaDateFormat(this.props.date)}
      </div>
    );
  }
}

class MessageListFragment extends React.Component<{
  currentUserId: string;
  date: string;
  messages: MessageData[];
  toggleLike: Function;
  edit: Function;
  delete: Function;
}> {
  render() {
    return (
      <>
        <MessageDivider date={this.props.date} />
        {this.props.messages.map(message =>
          message.userId === this.props.currentUserId ? (
            <OwnMessage
              key={message.id}
              id={message.id}
              text={message.text}
              time={
                message.editedAt
                  ? getTime(message.editedAt)
                  : getTime(message.createdAt)
              }
              edit={this.props.edit}
              delete={this.props.delete}
            />
          ) : (
            <Message
              key={message.id}
              id={message.id}
              avatar={message.avatar}
              user={message.user}
              text={message.text}
              time={
                message.editedAt
                  ? getTime(message.editedAt)
                  : getTime(message.createdAt)
              }
              liked={message.liked}
              toggleLike={this.props.toggleLike}
            />
          ),
        )}
      </>
    );
  }
}

function splitByDates(messages: MessageData[]): Array<[string, MessageData[]]> {
  const messagesByDate: Map<string, MessageData[]> = new Map();
  const result: Array<[string, MessageData[]]> = [];
  messages.forEach(message => {
    const dateFormated = new Intl.DateTimeFormat('en-US', {
      dateStyle: 'full',
    }).format(new Date(message.createdAt));
    if (messagesByDate.has(dateFormated)) {
      const arr = messagesByDate.get(dateFormated) as MessageData[];
      arr.push(message);
      messagesByDate.set(dateFormated, arr);
    } else {
      messagesByDate.set(dateFormated, [message]);
    }
  });
  messagesByDate.forEach((messages, date) => {
    result.push([date, messages]);
  });
  return result;
}

function getTime(isoDate: string) {
  return new Date(isoDate).toTimeString().slice(0, 5);
}

function bsaDateFormat(fullDate: string): string {
  const [day, monthDate] = fullDate.split(', ');
  const [month, date] = monthDate.split(' ');
  return `${day}, ${date} ${month}`;
}
