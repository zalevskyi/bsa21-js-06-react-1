import React from 'react';
import '../styles/Header.css';

interface HeaderProps {
  title: string;
  usersCount: number;
  messagesCount: number;
  lastMessageDate: string;
}

export class Header extends React.Component<HeaderProps> {
  render() {
    return (
      <div className='header'>
        <Title title={this.props.title} />
        <UsersCount count={this.props.usersCount} />
        <MessagesCount count={this.props.messagesCount} />
        <LastMessageDate lastDate={this.props.lastMessageDate} />
      </div>
    );
  }
}

class Title extends React.Component<{ title: string }> {
  render() {
    return <div className='header-title'>{this.props.title}</div>;
  }
}

class UsersCount extends React.Component<{ count: number }> {
  render() {
    return (
      <div className='header-users-count'>
        {this.props.count} User{this.props.count !== 1 && 's'}
      </div>
    );
  }
}

class MessagesCount extends React.Component<{ count: number }> {
  render() {
    return (
      <div className='header-messages-count'>
        {this.props.count} Message{this.props.count !== 1 && 's'}
      </div>
    );
  }
}

class LastMessageDate extends React.Component<{ lastDate: string }> {
  render() {
    return (
      <div className='header-last-message-date'>
        Last message at: {formatDate(this.props.lastDate)}
      </div>
    );
  }
}

function formatDate(isoDate: string): string {
  // ISO format (ISO 8601), can be 24-27 characters long
  // YYYY-MM-DDTHH:mm:ss.sssZ or ±YYYYYY-MM-DDTHH:mm:ss.sssZ
  const [datePart, timePart] = isoDate.split('T');
  const ISO_10_CHAR = 10;
  const char_shift = datePart.length - ISO_10_CHAR;
  const year = datePart.slice(0 + char_shift, 4 + char_shift);
  const month = datePart.slice(5 + char_shift, 7 + char_shift);
  const day = datePart.slice(8 + char_shift, 10 + char_shift);
  const time = timePart.slice(0, 5);
  return `${day}.${month}.${year} ${time}`;
}
