import React from 'react';
import '../styles/Preloader.css';

export class Preloader extends React.Component {
  render() {
    return <div className='preloader'>Preloader</div>;
  }
}
