import React from 'react';
import { Preloader } from './Preloader';
import { LoadError } from './LoadError';
import { Header } from './Header';
import '../styles/Chat.css';
import { MessageList } from './MessageList';
import { MessageInputUpdate } from './MessageInputUpdate';
import { MessageInputSend } from './MessageInputSend';

interface ChatProps {
  url: string;
}

export interface MessageData {
  avatar: string;
  createdAt: string;
  editedAt: string;
  id: string;
  text: string;
  user: string;
  userId: string;
  liked?: boolean;
}

export class Chat extends React.Component<ChatProps> {
  state: {
    loading: boolean;
    error: string | null;
    messages: MessageData[];
    currentUserId: string;
    currentUserName: string;
    editMessageId: string | null;
    editMessageText: string | null;
  };
  constructor(props: ChatProps) {
    super(props);
    this.state = {
      loading: true,
      error: null,
      messages: [],
      currentUserId: 'me',
      currentUserName: 'me',
      editMessageId: null,
      editMessageText: null,
    };
    this.toggleLike = this.toggleLike.bind(this);
    this.edit = this.edit.bind(this);
    this.delete = this.delete.bind(this);
    this.update = this.update.bind(this);
    this.send = this.send.bind(this);
  }
  render() {
    return (
      <div className='chat'>
        {this.state.loading ? (
          <Preloader />
        ) : this.state.error ? (
          <LoadError message={this.state.error} />
        ) : (
          <>
            <Header
              title='Finance chat'
              usersCount={this.usersCount()}
              messagesCount={this.state.messages.length}
              lastMessageDate={
                this.state.messages[this.state.messages.length - 1].createdAt
              }
            />
            <MessageList
              currentUserId={this.state.currentUserId}
              messages={this.state.messages}
              toggleLike={this.toggleLike}
              edit={this.edit}
              delete={this.delete}
            />
            {this.state.editMessageId && this.state.editMessageText ? (
              <MessageInputUpdate
                id={this.state.editMessageId}
                text={this.state.editMessageText}
                update={this.update}
              />
            ) : (
              <MessageInputSend send={this.send} />
            )}
          </>
        )}
      </div>
    );
  }
  componentDidMount() {
    this.loadData();
  }
  async loadData() {
    this.setState({ loading: true, error: null, messages: [] });
    const response = await fetch(this.props.url);
    if (!response.ok) {
      console.log('loading error');
      this.setState({
        loading: false,
        error: `Loadding error ${response.status}`,
      });
    } else {
      const responseJSON = await response.json();
      responseJSON.sort(sortByCreatedDate);
      this.setState({ loading: false, messages: responseJSON });
    }
  }
  toggleLike(id: string) {
    const newMessages = this.state.messages;
    newMessages.forEach(message => {
      if (message.id === id) {
        message.liked = !message.liked;
      }
    });
    this.setState({ messages: newMessages });
  }
  edit(id: string) {
    let text: string = '';
    this.state.messages.forEach(message => {
      if (message.id === id) {
        text = message.text;
      }
    });
    this.setState({ editMessageId: id, editMessageText: text });
  }
  update(id: string, text: string) {
    this.setState({
      messages: this.state.messages.map(message => {
        if (message.id === id) {
          message.text = text;
          message.editedAt = new Date().toISOString();
        }
        return message;
      }),
      editMessageId: null,
      editMessageText: null,
    });
  }
  send(text: string) {
    const newMessages = this.state.messages;
    const now = new Date();
    newMessages.push({
      avatar: '',
      createdAt: now.toISOString(),
      editedAt: '',
      id: `${this.state.currentUserId}-${now.toISOString()}`,
      text: text,
      user: this.state.currentUserName,
      userId: this.state.currentUserId,
    });
    this.setState({
      messages: newMessages,
    });
  }
  delete(id: string) {
    this.setState({
      messages: this.state.messages.filter(message => message.id !== id),
    });
  }
  usersCount() {
    const uniqueUsers = new Set();
    this.state.messages.forEach(message => {
      if (!uniqueUsers.has(message.userId)) {
        uniqueUsers.add(message.userId);
      }
    });
    return uniqueUsers.size;
  }
}

function sortByCreatedDate(a: MessageData, b: MessageData) {
  if (a.createdAt > b.createdAt) {
    return 1;
  } else if (a.createdAt < b.createdAt) {
    return -1;
  }
  return 0;
}
