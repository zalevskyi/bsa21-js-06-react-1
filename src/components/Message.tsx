import React from 'react';
import '../styles/Message.css';
import { BasicMessage } from './BasicMessage';

export class Message extends React.Component<{
  id: string;
  avatar: string;
  user: string;
  text: string;
  time: string;
  liked?: boolean;
  toggleLike: Function;
}> {
  render() {
    return (
      <div className='message'>
        <Avatar avatar={this.props.avatar} />
        <User user={this.props.user} />
        <BasicMessage text={this.props.text} time={this.props.time} />
        <Like
          id={this.props.id}
          liked={this.props.liked}
          toggleLike={this.props.toggleLike}
        />
      </div>
    );
  }
}

class Avatar extends React.Component<{ avatar: string }> {
  render() {
    return (
      <img
        className='message-user-avatar'
        src={this.props.avatar}
        alt='User avatar'
      />
    );
  }
}

class User extends React.Component<{ user: string }> {
  render() {
    return <div className='message-user-name'>{this.props.user}</div>;
  }
}

class Like extends React.Component<{
  id: string;
  liked?: boolean;
  toggleLike: Function;
}> {
  state: { liked: boolean };
  constructor(props: { id: string; liked?: boolean; toggleLike: Function }) {
    super(props);
    this.state = { liked: this.props.liked || false };
    this.onChange = this.onChange.bind(this);
  }
  render() {
    return (
      <input
        className={this.props.liked ? 'message-liked' : 'message-like'}
        type='checkbox'
        checked={this.state.liked}
        onChange={this.onChange}
      />
    );
  }
  onChange() {
    this.setState({ liked: !this.state.liked });
    this.props.toggleLike(this.props.id);
  }
}
